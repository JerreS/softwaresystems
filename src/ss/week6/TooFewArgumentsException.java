package ss.week6;

public class TooFewArgumentsException extends WrongArgumentException {
	public TooFewArgumentsException() {
		super("Too few arguments specified.");
	}
}

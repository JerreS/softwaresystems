package ss.week6;

import java.util.Scanner;

public class Words {
	public static void main(String[] args) {
		try (Scanner scanner = new Scanner(System.in)) {

			boolean continueLoop = true;
			while (continueLoop) {
				System.out.print("Line (or \"end\"): ");

				if (scanner.hasNextLine()) {
					String line = scanner.nextLine();
					int wordCount = 0;

					try (Scanner subScanner = new Scanner(line)) {
						while (subScanner.hasNext()) {
							String currentWord = subScanner.next();
							if (currentWord.equals("end")) {
								continueLoop = false;
							} else {
								wordCount++;
								System.out.println("Word " + wordCount + ": " + currentWord);
							}
						}
					}
				}
			}
		}

	}
}

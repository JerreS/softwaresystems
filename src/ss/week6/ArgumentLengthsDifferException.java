package ss.week6;

public class ArgumentLengthsDifferException extends WrongArgumentException {

	public ArgumentLengthsDifferException(int x, int y) {
		super("Expected " + x + " arguments but received " + y + ".");
	}

}

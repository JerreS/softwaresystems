package ss.week6.voteMachine;

import java.util.List;
import java.util.Map;

import ss.week6.voteMachine.gui.VoteGUIView;

public class VoteMachine {

	private PartyList partyList;
	private VoteList voteList;

	private VoteView view;

	public VoteMachine() {
		partyList = new PartyList();
		voteList = new VoteList();
		view = new VoteGUIView(this);
		partyList.addObserver(view);
		voteList.addObserver(view);
	}

	public void addParty(String party) {
		partyList.addParty(party);
	}

	public void vote(String party) {
		if (!partyList.hasParty(party)) {
			view.showError("Party is not added!");
		} else {
			voteList.addVote(party);
		}
	}

	public void getParties() {
		view.showParties(partyList.getParties());
	}

	public void getVotes() {
		view.showVotes(voteList.getVotes());
	}

	public void start() {
		view.start();

	}

	public static void main(String[] args) {
		VoteMachine machine = new VoteMachine();
		machine.start();
	}
}

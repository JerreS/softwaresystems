package ss.week6.voteMachine;

import java.util.HashMap;
import java.util.Map;
import java.util.Observable;

public class VoteList extends Observable {

	private Map<String, Integer> votes;

	public VoteList() {
		votes = new HashMap<>();
	}

	public void addVote(String party) {
		int initial = 0;
		if (votes.containsKey(party)) {
			initial = votes.get(party);
		}
		votes.put(party, initial + 1);
		setChanged();
		notifyObservers("vote");
	}

	public Map<String, Integer> getVotes() {
		return votes;
	}
}

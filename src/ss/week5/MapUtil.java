package ss.week5;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class MapUtil {

	// @ ensures \result == (\forall K x1, x2; map.keySet().contains(x1) &&
	// map.keySet().contains(x2) && x1 != x2; map.get(x1) != map.get(x2));
	// @ pure
	public static <K, V> boolean isOneOnOne(Map<K, V> map) {

		if (map == null) {
			return false;
		}

		for (K x1 : map.keySet()) {
			for (K x2 : map.keySet()) {
				if (x1 != x2 && map.get(x1) == map.get(x2)) {
					return false;
				}
			}
		}

		return true;
	}

	// @ ensures \result == (\forall V value; range.contains(value) &&
	// map.containsValue(value));
	// @ pure
	public static <K, V> boolean isSurjectiveOnRange(Map<K, V> map, Set<V> range) {

		if (map == null || range == null) {
			return false;
		}

		for (V value : range) {
			if (!map.containsValue(value)) {
				return false;
			}
		}

		return true;
	}

	// @ ensures (\forall V value; \result.containsKey(value) && (\forall K key;
	// \result.get(value).contains(key) && map.get(key) == value) );
	// @ pure
	public static <K, V> Map<V, Set<K>> inverse(Map<K, V> map) {

		if (map == null) {
			return null;
		}

		Map<V, Set<K>> result = new HashMap<V, Set<K>>();

		for (V value : map.values()) {
			Set<K> keys = new HashSet<K>();

			for (K key : map.keySet()) {
				if (map.get(key) == value) {
					keys.add(key);
				}
			}

			result.put(value, keys);
		}

		return result;
	}

	// @ requires isOneOnOne(map) && isSurjectiveOnRange(map, new
	// HashSet<V>(map.values()));
	// @ ensures (\forall V value; \result.containsKey(value) && (\exists K key;
	// map.containsKey(key) && map.get(key) == value));
	// @ pure
	public static <K, V> Map<V, K> inverseBijection(Map<K, V> map) {

		if (map == null) {
			return null;
		}

		Map<V, K> result = new HashMap<V, K>();

		for (V value : map.values()) {
			for (K key : map.keySet()) {
				if (map.get(key) == value) {
					result.put(value, key);
					break;
				}
			}
		}

		return result;
	}

	// @ requires f != null && g != null;
	// @ ensures (\forall K key; f.containsKey(key) && g.containsKey(key));
	// @ pure
	public static <K, V, W> boolean compatible(Map<K, V> f, Map<V, W> g) {

		if (f == null || g == null) {
			return false;
		}

		for (V value : f.values()) {
			if (!g.containsKey(value)) {
				return false;
			}
		}

		return true;
	}

	// @ requires compatible(f, g);
	// @ ensures (\forall K key; \result.containsKey(key) && \result.get(key) ==
	// g.get(f.get(key)));
	// @ pure
	public static <K, V, W> Map<K, W> compose(Map<K, V> f, Map<V, W> g) {

		if (f == null || g == null) {
			return null;
		}

		Map<K, W> composedMap = new HashMap<K, W>();

		for (Map.Entry<K, V> entry : f.entrySet()) {
			composedMap.put(entry.getKey(), g.get(entry.getValue()));
		}

		return composedMap;
	}
}

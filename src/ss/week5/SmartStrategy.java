package ss.week5;

public class SmartStrategy implements Strategy {

	private NaiveStrategy naiveStrategy;

	public SmartStrategy() {
		naiveStrategy = new NaiveStrategy();
	}

	@Override
	public String getName() {
		return "Smart";
	}

	@Override
	public int determineMove(Board b, Mark m) {

		int middle = b.index(Board.DIM / 2, Board.DIM / 2);
		if (b.isEmptyField(middle)) {
			return middle;
		}

		for (int i = 0; i < Board.DIM * Board.DIM; i++) {
			Board copy = b.deepCopy();

			if (copy.isEmptyField(i)) {
				copy.setField(i, m);
				if (copy.isWinner(m)) {
					return i;
				}
			}
		}

		for (int i = 0; i < Board.DIM * Board.DIM; i++) {
			Board copy = b.deepCopy();

			if (copy.isEmptyField(i)) {
				Mark opponent = m == Mark.OO ? Mark.XX : Mark.OO;
				copy.setField(i, opponent);
				if (copy.isWinner(opponent)) {
					return i;
				}
			}
		}

		return naiveStrategy.determineMove(b, m);
	}

}

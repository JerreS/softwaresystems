package ss.week5;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;

/**
 * A simple class that experiments with the Hex encoding of the Apache Commons
 * Codec library.
 *
 */
public class EncodingTest {
	public static void main(String[] args) throws DecoderException {
		String input = "Hello World";

		// hex
		String encodedString = Hex.encodeHexString(input.getBytes());
		System.out.println(encodedString);
		System.out.println(new String(Hex.decodeHex(encodedString.toCharArray())));

		System.out.println();

		// 5.13.1
		System.out.println("encodeBase64String(\"Hello World\")");
		encodedString = Base64.encodeBase64String(input.getBytes());
		System.out.println(encodedString);

		System.out.println();

		// 5.13.2
		System.out.println("Base64.encodeBase64String(Hex.decodeHex(\"010203040506\".toCharArray()))");
		encodedString = Base64.encodeBase64String(Hex.decodeHex("010203040506".toCharArray()));
		System.out.println(encodedString);

		System.out.println();

		// 5.13.3
		System.out.println("new String(Base64.decodeBase64(\"U29mdHdhcmUgU3lzdGVtcw==\"))");
		System.out.println(new String(Base64.decodeBase64("U29mdHdhcmUgU3lzdGVtcw==")));

		System.out.println();

		System.out.println("Base64.encodeBase64String(current.getBytes())");
		String current = "";
		for (int i = 0; i <= 10; i++) {

			current += "a";
			System.out.println(Base64.encodeBase64String(current.getBytes()));
		}
	}
}

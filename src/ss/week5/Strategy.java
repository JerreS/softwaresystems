package ss.week5;

public interface Strategy {
	/**
	 * Gets the name of the strategy.
	 * 
	 * @return A string representing the name of this strategy.
	 */
	public String getName();

	/**
	 * Determines the next move this strategy follows according to the current
	 * board state.
	 * 
	 * @param b
	 *            The board to read the state from.
	 * @param m
	 *            The mark of the player to determine the next move of.
	 * @return An integer representing the index of the field to set.
	 */
	public int determineMove(Board b, Mark m);
}

package ss.week5;

public class ComputerPlayer extends Player {

	private Strategy strategy;

	public ComputerPlayer(Mark mark) {
		this(mark, new NaiveStrategy());
	}

	public ComputerPlayer(Mark mark, Strategy strategy) {
		super(strategy.getName() + "-computer-" + mark.toString(), mark);
		this.setStrategy(strategy);
	}

	public Strategy getStrategy() {
		return strategy;
	}

	public void setStrategy(Strategy strategy) {
		this.strategy = strategy;
	}

	@Override
	public int determineMove(Board board) {
		return getStrategy().determineMove(board, this.getMark());
	}

}

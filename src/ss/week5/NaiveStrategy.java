package ss.week5;

import java.util.*;

public class NaiveStrategy implements Strategy {

	@Override
	public String getName() {
		return "Naive";
	}

	@Override
	public int determineMove(Board b, Mark m) {

		if (b.gameOver()) {
			return -1;
		}

		List<Integer> emptyFieldLists = new ArrayList<Integer>();

		for (int i = 0; i < Board.DIM * Board.DIM; i++) {
			if (b.isEmptyField(i)) {
				emptyFieldLists.add(i);
			}
		}

		return emptyFieldLists.get((int) (Math.random() * emptyFieldLists.size()));
	}

}

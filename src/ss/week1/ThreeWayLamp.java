package ss.week1;

public class ThreeWayLamp {

	public static final int OFF = 0;
	public static final int LOW = 1;
	public static final int MEDIUM = 2;
	public static final int HIGH = 3;

	private int setting = OFF;

	public ThreeWayLamp() {
	}

	public int getSetting() {
		return setting;
	}

	// public void setSetting(int setting) {
	// this.setting = setting;
	// }

	public void switchSetting() {
		setting = (setting + 1) % 4;
	}

}

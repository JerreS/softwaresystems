package ss.week1;

public class Password {

	public static final String INITIAL = "abcdefghijklmnopqrstuvwxyz";

	private String password;

	public Password() {
		password = INITIAL;
	}

	public boolean acceptable(String suggestion) {
		return (suggestion.length() >= 6) && !suggestion.contains(" ");
	}

	public boolean setWord(String oldpass, String newpass) {
		if (testWord(oldpass) && acceptable(newpass)) {
			password = newpass;
			return true;
		}
		return false;
	}

	public boolean testWord(String test) {
		return test.equals(password);
	}
}

package ss.week4.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import ss.week4.math.Function;
import ss.week4.math.Polynomial;

public class PolynomialTest {

	private static final double DELTA = 1e-15;

	private Polynomial function;

	@Before
	public void setUp() {
		function = new Polynomial(new double[] { 1, 2, 3 });
	}

	@Test
	public void testApply() {
		assertEquals(38, function.apply(5), DELTA);
	}

	@Test
	public void testDerivative() {
		Function derivative = function.derivative();
		assertEquals(12, derivative.apply(5), DELTA);
	}

	@Test
	public void testIntegrand() {
		Function integrand = function.integrand();
		assertEquals(81.0 + 2.0 / 3.0, integrand.apply(5), 0.00000001);
	}

}

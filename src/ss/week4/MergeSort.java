package ss.week4;

import java.util.*;

public class MergeSort {
	public static <Elem extends Comparable<Elem>> void mergesort(List<Elem> list) {

		if (list.size() == 0 || list.size() == 1)
			return;

		ArrayList<Elem> first = new ArrayList<Elem>(list.subList(0, list.size() / 2));
		ArrayList<Elem> second = new ArrayList<Elem>(list.subList(list.size() / 2, list.size()));

		mergesort(first);
		mergesort(second);

		int fi = 0;
		int si = 0;

		list.clear();
		while (fi < first.size() && si < second.size()) {
			if (first.get(fi).compareTo(second.get(si)) < 0) {
				list.add(first.get(fi));
				fi++;
			} else {
				list.add(second.get(si));
				si++;
			}
		}

		if (fi < first.size()) {
			list.addAll(first.subList(fi, first.size()));
		} else if (si < second.size()) {
			list.addAll(second.subList(si, second.size()));
		}

	}
}

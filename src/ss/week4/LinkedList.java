package ss.week4;

public class LinkedList<Element> {

	private /* @ spec_public @ */ int size;
	private Node first;

	// @ ensures \result.size == 0;
	public LinkedList() {
		size = 0;
		first = null;
	}

	public void add(int index, Element element) {
		Node newNode = new Node(element);
		if (index == 0) {
			newNode.next = first;
			first = newNode;
		} else {
			Node p = getNode(index - 1);
			newNode.next = p.next;
			p.next = newNode;
		}
		size = size + 1;
	}

	// @ ensures this.size == \old(size) - 1;
	public void remove(Element element) {
		Node previous = findBefore(element);

		if (previous != null) {
			// element is found and preceded by at least one node.
			Node oldNode = previous.next;
			previous.next = oldNode.next;
			oldNode.next = null;
		} else {
			// element not found or element is first node.
			if (first != null && first.element == element) {
				// element is first node.
				Node newFirst = first.next;
				first.next = null;
				first = newFirst;
			} else {
				// element is not present in list.
				return;
			}
		}

		size--;
	}

	public Node findBefore(Element element) {
		Node last = null;

		Node p = first;
		while (p != null && p.element != element) {
			last = p;
			p = p.next;
		}

		return last;
	}

	// @ requires 0 <= index && index < this.size();
	public Element get(int index) {
		Node p = getNode(index);
		return p.element;
	}

	// @ requires 0 <= i && i < this.size();
	private /* @ pure @ */ Node getNode(int i) {
		Node p = first;
		int pos = 0;
		while (pos != i) {
			p = p.next;
			pos = pos + 1;
		}
		return p;
	}

	// @ ensures \result >= 0;
	public /* @ pure @ */ int size() {
		return size;
	}

	public class Node {
		private Element element;
		public Node next;

		public Node(Element element) {
			this.element = element;
			this.next = null;
		}

		public Element getElement() {
			return element;
		}
	}
}

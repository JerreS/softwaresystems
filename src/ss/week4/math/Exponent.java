package ss.week4.math;

public class Exponent implements Function, Integrandable {

	private int power;

	public Exponent(int power) {
		this.power = power;
	}

	public int getPower() {
		return power;
	}

	@Override
	public double apply(double value) {
		return Math.pow(value, power);
	}

	@Override
	public Function derivative() {
		int newPower = power - 1;
		if (newPower == 0) {
			return new Constant(power);
		}
		return new LinearProduct(power, new Exponent(power - 1));
	}

	@Override
	public String toString() {
		switch (power) {
		case 0:
			return "1";
		case 1:
			return "x";
		default:
			return "x^" + power;
		}
	}

	@Override
	public Function integrand() {
		return new LinearProduct(1.0 / (getPower() + 1.0), new Exponent(getPower() + 1));
	}

}

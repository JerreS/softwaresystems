package ss.week4.math;

public interface Integrandable {
	/**
	 * Determines and returns the integrand of the function.
	 * 
	 * @return A function representing an integrand.
	 */
	public Function integrand();
}

package ss.week4.math;

public class LinearProduct extends Product implements Integrandable {

	public LinearProduct(double scalar, Function function) {
		this(new Constant(scalar), function);
	}

	public LinearProduct(Constant scalar, Function function) {
		super(scalar, function);
	}

	public Constant getScalar() {
		return (Constant) this.getLeft();
	}

	public Function derivative() {
		return new LinearProduct(getScalar(), this.getRight().derivative());
	}

	@Override
	public Function integrand() {
		return new LinearProduct(getScalar(), ((Integrandable) this.getRight()).integrand());
	}
}

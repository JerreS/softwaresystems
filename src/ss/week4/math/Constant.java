package ss.week4.math;

public class Constant implements Function, Integrandable {

	private double value;

	public Constant(double value) {
		this.value = value;
	}

	public double getValue() {
		return value;
	}

	@Override
	public double apply(double value) {
		return this.value;
	}

	@Override
	public Function derivative() {
		return new Constant(0);
	}

	@Override
	public String toString() {
		return String.valueOf(value);
	}

	@Override
	public Function integrand() {
		return new LinearProduct(value, new Exponent(1));
	}
}

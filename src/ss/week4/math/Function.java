package ss.week4.math;

public interface Function {

	/**
	 * Applies the function using a given value.
	 * 
	 * @param value
	 *            The value to apply the function to.
	 * @return The result of the function.
	 */
	public double apply(double value);

	/**
	 * Determines and returns the derivative of the current function.
	 * 
	 * @return A function representing the derivative.
	 */
	public Function derivative();
}

package ss.week4.math;

public class Polynomial implements Function, Integrandable {

	private LinearProduct[] parts;

	public Polynomial(double[] scalars) {
		parts = new LinearProduct[scalars.length];
		for (int i = 0; i < scalars.length; i++) {
			parts[i] = new LinearProduct(scalars[i], new Exponent(scalars.length - i - 1));
		}
	}

	@Override
	public double apply(double value) {
		double total = 0;
		for (int i = 0; i < parts.length; i++) {
			total += parts[i].apply(value);
		}
		return total;
	}

	@Override
	public Function derivative() {

		double[] scalars = new double[parts.length - 1];

		for (int i = 0; i < parts.length - 1; i++) {
			scalars[i] = parts[i].getScalar().getValue() * ((Exponent) parts[i].getRight()).getPower();
		}

		return new Polynomial(scalars);
	}

	@Override
	public Function integrand() {

		double[] scalars = new double[parts.length + 1];

		for (int i = 0; i < parts.length; i++) {
			scalars[i] = parts[i].getScalar().getValue() / (((Exponent) parts[i].getRight()).getPower() + 1);
		}

		return new Polynomial(scalars);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < parts.length; i++) {
			builder.append(parts[i].toString());
			if (i < parts.length - 1)
				builder.append(" + ");
		}
		return builder.toString();
	}

}

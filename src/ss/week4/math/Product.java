package ss.week4.math;

public class Product implements Function {

	private Function left;
	private Function right;

	public Product(Function left, Function right) {
		this.left = left;
		this.right = right;
	}

	public Function getLeft() {
		return left;
	}

	public Function getRight() {
		return right;
	}

	@Override
	public double apply(double value) {
		return left.apply(value) * right.apply(value);
	}

	@Override
	public Function derivative() {
		return new Sum(new Product(left.derivative(), right), new Product(left, right.derivative()));
	}

	@Override
	public String toString() {
		return left.toString() + " * " + right.toString();
	}

}

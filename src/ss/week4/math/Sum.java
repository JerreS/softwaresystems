package ss.week4.math;

public class Sum implements Function, Integrandable {

	private Function left;
	private Function right;

	public Sum(Function left, Function right) {
		this.left = left;
		this.right = right;
	}

	public Function getLeft() {
		return left;
	}

	public Function getRight() {
		return right;
	}

	@Override
	public double apply(double value) {
		return left.apply(value) + right.apply(value);
	}

	@Override
	public Function derivative() {
		return new Sum(left.derivative(), right.derivative());
	}

	@Override
	public String toString() {
		return left.toString() + " + " + right.toString();
	}

	@Override
	public Function integrand() {
		return new Sum(((Integrandable) left).integrand(), ((Integrandable) right).integrand());
	}

}

package ss.week2;

public class Rectangle {

	// @ private invariant length >= 0;
	private int length;
	// @ private invariant width >= 0;
	private int width;

	// @ requires length >= 0;
	// @ requires width >= 0;
	// @ ensures length() == length;
	// @ ensures width() == width;
	public Rectangle(int length, int width) {
		assert (length >= 0 && width >= 0);
		this.length = length;
		this.width = width;
		assert (length() == length);
		assert (width() == width);
	}

	// @ ensures \result >= 0;
	// @ pure
	public int length() {
		assert (length >= 0);
		return length;
	}

	// @ ensures \result >= 0;
	// @ pure
	public int width() {
		assert (width >= 0);
		return width;
	}

	// @ ensures \result == length() * width();
	// @ pure
	public int area() {
		assert (length >= 0 && width >= 0);
		return length * width;
	}

	// @ ensures \result == 2 * (length() + width());
	// @ pure
	public int perimeter() {
		assert (length >= 0 && width >= 0);
		return 2 * (length + width);
	}
}

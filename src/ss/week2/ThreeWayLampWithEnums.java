package ss.week2;

// TODO: add invariants n stuff

public class ThreeWayLampWithEnums {

	private ThreeWayLampState setting = ThreeWayLampState.OFF;

	// @ ensures getSetting() == setting;
	public ThreeWayLampWithEnums(ThreeWayLampState setting) {
		this.setting = setting;
		assert (getSetting() == setting);
	}

	// @ pure
	public ThreeWayLampState getSetting() {
		return setting;
	}

	// @ ensures \old(getSetting()) == ThreeWayLampState.OFF ==> getSetting() ==
	// ThreeWayLampState.LOW;
	// @ ensures \old(getSetting()) == ThreeWayLampState.LOW ==> getSetting() ==
	// ThreeWayLampState.MEDIUM;
	// @ ensures \old(getSetting()) == ThreeWayLampState.MEDIUM ==> getSetting()
	// == ThreeWayLampState.HIGH;
	// @ ensures \old(getSetting()) == ThreeWayLampState.HIGH ==> getSetting()
	// == ThreeWayLampState.OFF;
	public void switchSetting() {
		switch (this.setting) {
		case OFF:
			this.setting = ThreeWayLampState.LOW;
			assert (getSetting() == ThreeWayLampState.LOW);
			break;
		case LOW:
			this.setting = ThreeWayLampState.MEDIUM;
			assert (getSetting() == ThreeWayLampState.MEDIUM);
			break;
		case MEDIUM:
			this.setting = ThreeWayLampState.HIGH;
			assert (getSetting() == ThreeWayLampState.HIGH);
			break;
		case HIGH:
			this.setting = ThreeWayLampState.OFF;
			assert (getSetting() == ThreeWayLampState.OFF);
			break;
		}
	}
}

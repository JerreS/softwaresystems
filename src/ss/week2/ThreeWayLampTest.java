package ss.week2;

public class ThreeWayLampTest {

	public static void main(String[] args) {
		ThreeWayLampWithEnums lamp = new ThreeWayLampWithEnums(ThreeWayLampState.OFF);
		System.out.println(lamp.getSetting());
		lamp.switchSetting();
		System.out.println(lamp.getSetting());
		lamp.switchSetting();
		System.out.println(lamp.getSetting());
		lamp.switchSetting();
		System.out.println(lamp.getSetting());
		lamp.switchSetting();
		System.out.println(lamp.getSetting());
		lamp.switchSetting();
		System.out.println(lamp.getSetting());
	}
}

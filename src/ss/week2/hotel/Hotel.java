package ss.week2.hotel;

public class Hotel {

	// private invariant room1 != null;
	private Room room1;
	// private invariant room1 != null;
	private Room room2;
	// private invariant name != null;
	private String name;
	// private invariant password != null;
	private Password password;

	// @ requires name != null;
	// @ ensures getName() == name;
	public Hotel(String name) {
		assert (name != null);

		this.name = name;
		password = new Password();
		room1 = new Room(1);
		room2 = new Room(2);

		assert (getName() == name);
	}

	// @ ensures \result != null;
	// @ pure
	public String getName() {
		assert (name != null);
		return name;
	}

	// @ requires password != null && guestName != null;
	// @ ensures \result == null || (\result.getGuest() != null &&
	// \result.getGuest().getName() == guestName);
	public Room checkIn(String password, String guestName) {
		assert (password != null && guestName != null);

		if (!this.password.testWord(password))
			return null;

		Room room = getRoom(guestName);
		if (room != null)
			return null;

		room = getFreeRoom();
		if (room == null)
			return null;

		Guest guest = new Guest(guestName);
		guest.checkin(room);

		assert (room.getGuest().getName() == guestName);
		return room;
	}

	// @ requires guestName != null;
	// @ ensures \old(getRoom(guestName)).getSafe().isActive() == false;
	// @ ensures getRoom(guestName) == null;
	public void checkOut(String guestName) {
		assert (guestName != null);

		Room room = getRoom(guestName);
		if (room != null) {
			room.getSafe().deactivate();
			room.getGuest().checkout();

			assert (!room.getSafe().isActive());
			assert (room.getGuest() == null);
		}

		assert (getRoom(guestName) == null);
	}

	// @ ensures \result == null || \result.getGuest() != null;
	// @ pure
	public Room getFreeRoom() {
		if (room1.getGuest() == null)
			return room1;
		if (room2.getGuest() == null)
			return room2;
		return null;
	}

	// @ requires guestName != null;
	// @ ensures \result == null || \result.getGuest().getName() == guestName;
	// @ pure
	public Room getRoom(String guestName) {
		assert (guestName != null);

		Room room = null;

		if (room1.getGuest() != null && room1.getGuest().getName() == guestName) {
			room = room1;
		} else if (room2.getGuest() != null && room2.getGuest().getName() == guestName) {
			room = room2;
		}

		assert (room == null || room.getGuest().getName() == guestName);
		return room;
	}

	// @ ensures \result != null;
	// @ pure
	public Password getPassword() {
		assert (password != null);
		return password;
	}

	// @ pure
	public String toString() {
		return "Hotel " + getName() + ", " + room1.toString() + ", " + room2.toString();
	}
}

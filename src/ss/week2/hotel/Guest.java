package ss.week2.hotel;

public class Guest {

	private String name;
	private Room room;

	public Guest(java.lang.String n) {
		this.name = n;
	}

	public boolean checkin(Room r) {
		if (r == null || r.getGuest() != null)
			return false;
		room = r;
		r.setGuest(this);
		return true;
	}

	public boolean checkout() {
		if (room != null) {
			room.setGuest(null);
			room = null;
			return true;
		}
		return false;
	}

	// @ pure
	public java.lang.String getName() {
		return name;
	}

	// @ pure
	public Room getRoom() {
		return room;
	}

	public java.lang.String toString() {
		return "Guest Name: " + name + ", Room: " + room;
	}
}

package ss.week2.hotel;

public class Safe {

	private boolean isActive;
	private boolean isOpen;

	// @ private invariant password != null;
	private Password password;

	// @ requires password != null;
	// @ ensures getPassword() == password;
	public Safe(Password password) {
		assert (password != null);
		this.password = password;
		assert (getPassword() == password);
	}

	// @ pure
	public boolean isActive() {
		return isActive;
	}

	// @ pure
	public boolean isOpen() {
		return isOpen;
	}

	// @ pure
	public Password getPassword() {
		return password;
	}

	// @ requires password != null;
	// @ ensures getPassword().testWord(password) ==> isActive();
	public void activate(String password) {
		assert (password != null);
		if (this.password.testWord(password)) {
			isActive = true;
			assert (isActive());
		}
	}

	// @ ensures isOpen() == false;
	// @ ensures isActive() == false;
	public void deactivate() {
		close();
		isActive = false;
		assert (!isOpen());
		assert (!isActive());
	}

	// @ requires password != null;
	// @ ensures \old(isOpen()) ==> isOpen();
	// @ ensures (isActive() && getPassword().testWord(password)) ==> isOpen();
	public void open(String password) {
		assert (password != null);
		if (isActive && this.password.testWord(password)) {
			isOpen = true;
			assert (isOpen());
		}
	}

	// @ ensures isOpen() == false;
	public void close() {
		isOpen = false;
		assert (!isOpen());
	}

	public static void main(String[] args) {
		Password password = new Password();
		password.setWord(Password.INITIAL, "woofwoof1337");
		Safe safe = new Safe(null);
		safe.activate("lol");
	}
}

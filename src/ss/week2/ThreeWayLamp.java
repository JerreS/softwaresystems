package ss.week2;

public class ThreeWayLamp {

	public static final int OFF = 0;
	public static final int LOW = 1;
	public static final int MEDIUM = 2;
	public static final int HIGH = 3;

	// @ private invariant setting >= OFF && setting <= HIGH;
	private int setting = OFF;

	// @ requires setting >= 0 && setting <= HIGH;
	// @ ensures getSetting() == setting;
	public ThreeWayLamp(int setting) {
		assert (setting >= OFF && setting <= HIGH);
		this.setting = setting;
	}

	// @ ensures \result >= OFF && \result <= HIGH;
	// @ pure
	public int getSetting() {
		assert (setting >= OFF && setting <= HIGH);
		return setting;
	}

	// @ ensures \old(getSetting()) == OFF ==> getSetting() == LOW;
	// @ ensures \old(getSetting()) == LOW ==> getSetting() == MEDIUM;
	// @ ensures \old(getSetting()) == MEDIUM ==> getSetting() == HIGH;
	// @ ensures \old(getSetting()) == HIGH ==> getSetting() == OFF;
	public void switchSetting() {

		setting = (setting + 1) % 4;
	}

}

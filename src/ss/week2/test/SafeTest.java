package ss.week2.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import ss.week2.hotel.Safe;
import ss.week2.hotel.Password;

public class SafeTest {

	private static final String PASSWORD = "woofwoof1337";
	private Safe testSafe;

	@Before
	public void setUp() {
		Password password = new Password();
		password.setWord(Password.INITIAL, PASSWORD);
		testSafe = new Safe(password);
	}

	@Test
	public void testInitial() {
		assertEquals(false, testSafe.isActive());
		assertEquals(false, testSafe.isOpen());
	}

	@Test
	public void testActivateWrongPassword() {
		testSafe.activate("wrong password");
		assertEquals(false, testSafe.isActive());
	}

	@Test
	public void testActivateCorrectPassword() {
		testSafe.activate(PASSWORD);
		assertEquals(true, testSafe.isActive());
	}

	@Test
	public void testOpenWrongPassword() {
		testSafe.activate(PASSWORD);
		testSafe.open("wrong password");
		assertEquals(false, testSafe.isOpen());
	}

	@Test
	public void testOpenCorrectPassword() {
		testSafe.activate(PASSWORD);
		testSafe.open(PASSWORD);
		assertEquals(true, testSafe.isOpen());
	}

	@Test
	public void testOpenDeactivatedSafe() {
		testSafe.activate("wrong password");
		testSafe.open(PASSWORD);
		assertEquals(false, testSafe.isOpen());
	}

	@Test
	public void testDeactivateClosedSafe() {
		testSafe.activate(PASSWORD);
		assertEquals(true, testSafe.isActive());
		assertEquals(false, testSafe.isOpen());
		testSafe.deactivate();
		assertEquals(false, testSafe.isActive());
		assertEquals(false, testSafe.isOpen());
	}

	@Test
	public void testDeactivateOpenedSafe() {
		testSafe.activate(PASSWORD);
		testSafe.open(PASSWORD);
		assertEquals(true, testSafe.isActive());
		assertEquals(true, testSafe.isOpen());
		testSafe.deactivate();
		assertEquals(false, testSafe.isActive());
		assertEquals(false, testSafe.isOpen());
	}

}

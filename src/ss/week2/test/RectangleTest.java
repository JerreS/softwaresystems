package ss.week2.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import ss.week2.Rectangle;

public class RectangleTest {

	private Rectangle rectangle;

	@Before
	public void setUp() {
		rectangle = new Rectangle(100, 200);
	}

	@Test
	public void testInitial() {
		assertEquals(100, rectangle.length());
		assertEquals(200, rectangle.width());
	}

	@Test
	public void testArea() {
		assertEquals(20000, rectangle.area());
	}

	@Test
	public void testPermiter() {
		assertEquals(600, rectangle.perimeter());
	}
}

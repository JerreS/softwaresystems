package ss.week7.threads;

public class SynchronizedIntCell implements IntCell {

	private boolean isConsumed = true;
	private int value;
	
	@Override
	public synchronized void setValue(int val) {
		
		while (!isConsumed) {
			try {
				this.wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		isConsumed = false;
		value = val;
		this.notify();
	}

	@Override
	public synchronized int getValue() {
		
		while (isConsumed) { 
			try {
				this.wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		isConsumed = true;
		this.notify();
		return value;
	}

}

package ss.week7.threads;

public class TestConsole extends Thread {

	public TestConsole(String name) {
		super(name);
	}
	
	@Override
	public void run() {
		sum();
	}

	private void sum() {
		int x = Console.readInt(getName() + ": Number 1:");
		int y = Console.readInt(getName() + ": Number 2:");
		Console.println(String.format("%d + %d = %d", x, y, x + y));
	}
	
	public static void main(String[] args) {
		Thread thread1 = new TestConsole("Thread A");
		Thread thread2 = new TestConsole("Thread B");
		
		thread1.start();
		thread2.start();
		
		try {
			thread1.join();
			thread2.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}

package ss.week7.threads;

import java.util.concurrent.locks.*;

public class FinegrainedIntCell implements IntCell {

	private final Lock lock = new ReentrantLock();

	private boolean isConsumed = true;
	private final Condition consumed = lock.newCondition(); 
	private final Condition written = lock.newCondition(); 
	private int value;
	
	public FinegrainedIntCell() {
	}
	
	@Override
	public void setValue(int val) {
		lock.lock();
		try {
			try {
				while (!isConsumed) {
					consumed.await();
				}
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			value = val;
			isConsumed = false;
			written.signal();
		} finally {
			lock.unlock();
		}
	}

	@Override
	public int getValue() {
		int val = 0;
		lock.lock();
		try {
			try {
				while (isConsumed) {
					written.await();
				}
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			val = this.value;
			isConsumed = true;
			consumed.signal();
		} finally {
			lock.unlock();
		}
		return val;
	}

}

package ss.week7.threads;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class TestSyncLockConsole extends Thread {

	private static Lock lock = new ReentrantLock();
	
	public TestSyncLockConsole(String name) {
		super(name);
	}
	
	@Override
	public void run() {
		sum();
	}

	private synchronized void sum() {
		try {
			lock.lock();
			int x = SyncConsole.readInt(getName() + ": Number 1:");
			int y = SyncConsole.readInt(getName() + ": Number 2:");
			SyncConsole.println(String.format("%s: %d + %d = %d", getName(), x, y, x + y));
		} finally {
			lock.unlock();
		}
	}
	
	public static void main(String[] args) {
		Thread thread1 = new TestSyncConsole("Thread A");
		Thread thread2 = new TestSyncConsole("Thread B");
		
		thread1.start();
		thread2.start();
		
	}
}

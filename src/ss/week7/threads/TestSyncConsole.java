package ss.week7.threads;

public class TestSyncConsole extends Thread {

	private static Object lockObject = new Object();
	
	public TestSyncConsole(String name) {
		super(name);
	}
	
	@Override
	public void run() {
		sum();
	}

	private void sum() {
		synchronized (lockObject) {
			int x = SyncConsole.readInt(getName() + ": Number 1:");
			int y = SyncConsole.readInt(getName() + ": Number 2:");
			SyncConsole.println(String.format("%s: %d + %d = %d", getName(), x, y, x + y));
		}
	}
	
	public static void main(String[] args) {
		Thread thread1 = new TestSyncConsole("Thread A");
		Thread thread2 = new TestSyncConsole("Thread B");
		
		thread1.start();
		thread2.start();
		
	}
}

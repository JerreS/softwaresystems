package ss.week7.account;

public class MyThread extends Thread {

	private double amount;
	private int times;
	private Account account;

	public MyThread(double amount, int times, Account account) {
		this.amount = amount;
		this.times = times;
		this.account = account;		
	}
	
	@Override
	public void run() {
		for (int i = 0; i < times; i++) {
			account.transaction(amount);
		}
	}
}

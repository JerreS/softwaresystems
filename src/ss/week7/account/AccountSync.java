package ss.week7.account;

public class AccountSync {
	
	private static final int TIMES = 100000;
	public static void main(String[] args) {
		Account account = new Account();
		
		Thread thread1 = new MyThread(100, TIMES, account);
		Thread thread2 = new MyThread(-100, TIMES, account);
		
		thread1.start();
		thread2.start();
		
		try {
			thread1.join();
			thread2.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(account.getBalance());
	}
}

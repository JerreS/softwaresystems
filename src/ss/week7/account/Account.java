package ss.week7.account;

import java.util.concurrent.locks.*;

public class Account {
	private final Lock lock = new ReentrantLock();
	private final Condition aboveThousand = lock.newCondition();
	
	protected double balance = 0.0;

	public void transaction(double amount) {
		lock.lock();
		try {
			// correct?
			while (balance < -1000) {
				aboveThousand.await();
			}
			balance = balance + amount;
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			lock.unlock();
		}
	}
	
	public double getBalance() {
		return balance;

	}
}

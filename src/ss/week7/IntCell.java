package ss.week7;

public class IntCell {
    private int contents = 0;

    public void add(int amount) {
        contents = contents + amount;
    }
    public int get() {
        return contents;
    }

    public static void main(String[] args) {
        IntCell cell = new IntCell();
        Adder a1 = new Adder(cell, 1);
        Adder a2 = new Adder(cell, 2);
        a1.start();
        a2.start();
        try {
            a1.join();
            a2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(cell.get());
    }
}

class Adder extends Thread {
    private IntCell cell;
    private int amount;

    public Adder(IntCell cellArg, int amountArg) {
        this.cell = cellArg;
        this.amount = amountArg;
    }
    public void run() {
    	synchronized(cell) {
    		cell.add(amount);
    	}
    }
}

// answers to exercises 7.20:
// 1)   order of operation							| result
//      --------------------------------------------+----------
//		a1 read => a1 write => a2 read => a2 write  | 3
//		a1 & a2 read => a1 write => a2 write  		| 2
//		a1 & a2 read => a2 write => a1 write  		| 1
// 2) 3, sequential
// 3) 0 (threads aren't finished yet), and 1, 2, 3 for same reason as 1. 
// 4) no difference from 1. values can still be read by the two threads sequentially.
// 5) -

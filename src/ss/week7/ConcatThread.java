package ss.week7;

public class ConcatThread extends Thread {
    private static String text = ""; // global variable
    private String toe;

    public ConcatThread(String toeArg) {
        this.toe = toeArg;
    }

    public void run() {
    	synchronized (text) {
    		text = text.concat(toe);
    	}
    }

    public static void main(String[] args) {
        Thread a1 = new ConcatThread("one;");
        Thread a2 = new ConcatThread("two;");
        
        a1.start();
        a2.start();
        
        try {
			a1.join();
			a2.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
        
        System.out.println(text);
    }
}

// answers to exercices 7.21:
// 1) the line text = text.concat(toe); in the run method,
//	  since it involves read and write operations on the static variable text.
// 2) suppose the threads are called a1 and a2 respectively. 
// 		order of operation							| result
//		--------------------------------------------+----------
//		both a1 and a2 not finshed yet 				| ""
//		a1 finishes, a2 isn't						| "one;"
//		a2 finishes, a1 isn't						| "two;"
//		a1 read => a1 write => a2 read => a2 write  | "one;two;"
//		a1 & a2 read => a1 write => a2 write  		| "one;two;"
//		a1 & a2 read => a2 write => a1 write  		| "two;one;"
// 3) -
// 4) -
package ss.week3.test;

import org.junit.Before;
import org.junit.Test;

import ss.week3.hotel.BasicItem;
import ss.week3.hotel.Bill;
import ss.week3.hotel.Bill.Item;

import static org.junit.Assert.assertEquals;

public class BillTest {

	private Bill bill;

	@Before
	public void setUp() {
		bill = new Bill(null);
	}

	@Test
	public void testNoItems() {
		assertEquals(0, bill.getSum(), 0);
	}

	@Test
	public void testOneItem() {
		assertEquals("initial getSum == 0", bill.getSum() == 0, true);
		Item item = new BasicItem("Test", 13.37);
		bill.newItem(item);
		assertEquals("initial getSum == item.getAmount()", bill.getSum() == item.getAmount(), true);
	}

	@Test
	public void testMultipleItems() {
		assertEquals("initial getSum == 0", bill.getSum() == 0, true);
		Item item = new BasicItem("Test", 13.37);
		Item item2 = new BasicItem("Test2", 20.20);
		bill.newItem(item);
		bill.newItem(item2);
		assertEquals("initial getSum == item.getAmount()", bill.getSum() == item.getAmount() + item2.getAmount(), true);
	}

}

package ss.week3.pw;

public interface Criterion {

	/**
	 * Checks whether the given password meets the requirements.
	 * 
	 * @param password
	 *            the password to check.
	 * @return True if the password is acceptable, false otherwise.
	 */
	// @ requires password != null;
	public boolean acceptable(String password);

}

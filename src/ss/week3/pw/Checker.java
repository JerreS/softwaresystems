package ss.week3.pw;

public interface Checker {
	/**
	 * Checks whether the specified password is acceptable.
	 * 
	 * @param password
	 *            The password to check.
	 * @return True if the password is acceptable, otherwise false.
	 */
	// @ requires password != null;
	// @ pure
	public boolean acceptable(String password);

	/**
	 * Generates a new acceptable password.
	 * 
	 * @return A new acceptable password.
	 */
	public String generatePassword();
}

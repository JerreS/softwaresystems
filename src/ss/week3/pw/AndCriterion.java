package ss.week3.pw;

public class AndCriterion implements Criterion {

	private Criterion a;
	private Criterion b;

	// @ requires a != null && b != null;
	public AndCriterion(Criterion a, Criterion b) {
		assert (a != null && b != null);
		this.a = a;
		this.b = b;
	}

	@Override
	public boolean acceptable(String password) {
		assert (password != null);
		return a.acceptable(password) && b.acceptable(password);
	}

}

package ss.week3.pw;

public class RandomChecker implements Checker {

	private Checker checker;

	/**
	 * Creates a new instance of a random checker.
	 * 
	 * @param checker
	 *            the underlying checker to use.
	 */
	// @ requires checker != null;
	public RandomChecker(Checker checker) {
		assert (checker != null);
		this.checker = checker;
	}

	@Override
	public boolean acceptable(String password) {
		return checker.acceptable(password);
	}

	@Override
	public String generatePassword() {
		String password;
		do {
			password = Random.randomString(20);
		} while (!acceptable(password));
		return password;
	}

	public static void main(String[] args) {
		RandomChecker checker = new RandomChecker(new StrongChecker());
		System.out.println(checker.generatePassword());
	}
}

package ss.week3.pw;

public class CheckerCriterion implements Criterion {

	private Checker checker;

	// @ requires checker != null;
	public CheckerCriterion(Checker checker) {
		assert (checker != null);
		this.checker = checker;
	}

	@Override
	public boolean acceptable(String password) {
		assert (password != null);
		return checker.acceptable(password);
	}

}

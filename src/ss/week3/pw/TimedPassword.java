package ss.week3.pw;

public class TimedPassword extends Password {

	public static final int DEFAULT_EXPIRATION_TIME = 3;

	// @ private invariant validTime > 0;
	private long validTime;
	// @ private invariant expirationTime > 0;
	private int expirationTime;

	/**
	 * Creates a new instance of a TimedPassword, using the specified expiration
	 * time.
	 * 
	 * @param expirationTime
	 *            The expiration time in seconds.
	 */
	// @ requires expirationTime > 0;
	// @ ensures !isExpired();
	public TimedPassword(int expirationTime) {
		assert (expirationTime > 0);
		this.expirationTime = expirationTime;
		resetValidTime();
		assert (!isExpired());
	}

	/**
	 * Creates a new instance of a TimedPassword, using the default expiration
	 * time.
	 */
	// @ ensures !isExpired();
	public TimedPassword() {
		this(DEFAULT_EXPIRATION_TIME);
		assert (!isExpired());
	}

	/**
	 * Gets a value indicating whether the password's lifetime is expired.
	 * 
	 * @return true if the password is expired, false otherwise.
	 */
	// @ pure
	public boolean isExpired() {
		return java.lang.System.currentTimeMillis() > validTime;
	}

	// @ ensures isExpired() == false;
	@Override
	public boolean setWord(String oldpass, String newpass) {
		if (super.setWord(oldpass, newpass)) {
			resetValidTime();
			return true;
		}
		return false;
	}

	private void resetValidTime() {
		assert (expirationTime > 0);
		validTime = java.lang.System.currentTimeMillis() + (expirationTime * 1000);
		assert (validTime > 0);
	}
}

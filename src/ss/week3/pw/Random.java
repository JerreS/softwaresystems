package ss.week3.pw;

public class Random {

	/**
	 * Generates a random alphanumeric string with a given length.
	 * 
	 * @param length
	 *            the length of the string to generate.
	 * @return a random alphanumeric string.
	 */
	// @ requires length >= 0;
	public static String randomString(int length) {
		assert (length != 0);
		StringBuilder builder = new StringBuilder();

		for (int i = 0; i < length; i++) {
			if (Math.random() > 0.5)
				builder.append((char) ('0' + 10 * Math.random()));
			else
				builder.append((char) ('a' + 26 * Math.random()));
		}

		return builder.toString();
	}

	public static void main(String[] args) {
		for (int i = 0; i < 20; i++)
			System.out.println(randomString(i));
	}
}

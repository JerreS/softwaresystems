package ss.week3.pw;

public class BasicChecker implements Checker {

	public static final String INITPASS = "abcdefg";

	// @ ensures \result == (password.length() >= 6 && !password.contains(" "));
	@Override
	public boolean acceptable(String password) {
		assert (password != null);
		return password.length() >= 6 && !password.contains(" ");
	}

	@Override
	public String generatePassword() {
		return INITPASS;
	}

}

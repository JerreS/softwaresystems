package ss.week3.pw;

public class Password {

	public static final String INITIAL = "abcdefghijklmnopqrstuvwxyz";
	private String password;
	private Checker checker;
	private String factoryPassword;

	public Password(Checker checker) {
		this.checker = checker;
		this.password = (this.factoryPassword = checker.generatePassword());
	}

	public Password() {
		this(new BasicChecker());
		// password = INITIAL;

	}

	/**
	 * Gets the checker being used in this password.
	 * 
	 * @return the checker object.
	 */
	// @ ensures \result != null;
	// @ pure
	public Checker getChecker() {
		return checker;
	}

	/**
	 * Gets the initial or factory password of this password.
	 * 
	 * @return the factory password.
	 */
	// @ ensures \result != null;
	// @ pure
	public String getFactoryPassword() {
		return factoryPassword;
	}

	// @ pure
	public boolean acceptable(String suggestion) {
		return (suggestion.length() >= 6) && !suggestion.contains(" ");
	}

	public boolean setWord(String oldpass, String newpass) {
		if (testWord(oldpass) && acceptable(newpass)) {
			password = newpass;
			return true;
		}
		return false;
	}

	// @ pure
	public boolean testWord(String test) {
		return test.equals(password);
	}
}

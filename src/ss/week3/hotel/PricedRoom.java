package ss.week3.hotel;

public class PricedRoom extends Room implements Bill.Item {

	// @ private invariant roomPrice >= 0;
	private double roomPrice;

	/**
	 * Creates a new instance of a priced room, using a given room number, price
	 * and safe price.
	 * 
	 * @param roomNumber
	 *            The number of the room.
	 * @param roomPrice
	 *            The price per night of the room.
	 * @param safePrice
	 *            The price of the safe associated with this room.
	 */
	// @ requires roomNumber >= 0 && roomPrice >= 0 && safePrice >= 0;
	// @ ensures getAmount() == roomPrice;
	public PricedRoom(int roomNumber, double roomPrice, double safePrice) {
		super(roomNumber, new PricedSafe(safePrice));

		this.roomPrice = roomPrice;
	}

	@Override
	public double getAmount() {
		return roomPrice;
	}

	@Override
	public String toString() {
		return String.format("Room %d ($%.2f)", this.getNumber(), getAmount());
	}
}

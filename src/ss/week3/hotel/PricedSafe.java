package ss.week3.hotel;

public class PricedSafe extends Safe implements Bill.Item {

	// @ private invariant price >= 0;
	private double price;

	/**
	 * Creates a new instance of a PricedSafe, assigning it with the specified
	 * price.
	 * 
	 * @param price
	 *            The price to assign the safe with.
	 */
	public PricedSafe(double price) {
		super(new Password());
		this.price = price;
	}

	@Override
	public double getAmount() {
		return price;
	}

	@Override
	public String toString() {
		return String.format("Safe ($%.2f)", getAmount());
	}

}

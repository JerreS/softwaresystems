package ss.week3.hotel;

import java.io.PrintStream;

/**
 * Bill having multiple items.
 * 
 * @author Jerre
 *
 */
public class Bill {

	/**
	 * Abstraction of the items on the bill. Every item has an amount and
	 * description; the description is available with toString().
	 * 
	 * @author Jerre
	 */
	public static interface Item {
		/**
		 * Returns the amount.
		 * 
		 * @return the amount
		 */
		// @ pure
		double getAmount();
	}

	private PrintStream theOutStream;

	// @ private invariant sum >= 0;
	private double sum;

	/**
	 * Constructs a Bill sending the output to a given PrintStream.
	 * 
	 * @param theOutStream
	 *            output stream; or null if you don't want output.
	 */
	public Bill(PrintStream theOutStream) {
		this.theOutStream = theOutStream;
	}

	/**
	 * Print the sum of the bill on the output stream.
	 */
	public void close() {
		printLine("Total", getSum());
	}

	/**
	 * Returns the sum of the Bill.
	 * 
	 * @return the sum of the Bill.
	 */
	// @ pure
	public double getSum() {
		return sum;
	}

	/**
	 * Add an item to the Bill. If there is an output, the item will be printed
	 * on this output and the amount will be added to the sum of the Bill
	 * 
	 * @param item
	 *            The new item.
	 */
	// @ requires item != null;
	public void newItem(Item item) {
		assert (item != null);
		printLine(item.toString(), item.getAmount());
		sum += item.getAmount();
	}

	private void printLine(String text, double amount) {
		if (theOutStream != null) {
			theOutStream.println(String.format("%-20s $%.2f", text, amount));
		}
	}

}

package ss.week3.hotel;

public class BasicItem implements Bill.Item {

	// @ private invariant description != null;
	private String description;
	// @ private invariant amount >= 0;
	private double amount;

	// @ requires description != null && amount >= 0;
	// @ ensures toString() == description && getAmount() == amount;
	public BasicItem(String description, double amount) {
		assert (description != null && amount >= 0);
		this.description = description;
		this.amount = amount;
		assert (toString() != description && getAmount() >= amount);
	}

	@Override
	public double getAmount() {
		return amount;
	}

	// @ pure
	@Override
	public String toString() {
		return description;
	}

}
